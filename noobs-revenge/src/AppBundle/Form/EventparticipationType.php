<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class EventparticipationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userid = $options['userid'];

        $builder
            ->add('chosenroleid', EntityType::class, array(
                'label' => 'Rôle désiré',
                'class' => 'AppBundle:Characterrole',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ))
            ->add('characterid', EntityType::class, array(
                    'class' => 'AppBundle:Usercharacter',
                    //'property' => ''
                    'query_builder' => function(EntityRepository $er) use ($userid) {
                        return $er->createQueryBuilder("uc")
                            ->where("uc.userid = :userid")
                            ->orderBy("uc.main", "DESC")
                            ->setParameter('userid', $userid);
                    },
                    'multiple'  => false,
                    'expanded' => true,
                    'required' => true,
                )
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Eventparticipation',
            'userid' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_eventparticipation';
    }

}