<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

/**
 * Class EventType
 * @package AppBundle\Form
 */
class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('eventtypeid', EntityType::class, array(
                "label" => "Type d'événement",
                'class' => 'AppBundle:Eventtype',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => false,
            ))
            ->add('place', TextType::class, array(
                'label' => "Lieu de l'événement",
                'required' => true,
            ))
            ->add('title', TextType::class, array(
                'label' => "Titre de l'événement",
                'required' => false,
            ))
            ->add('datestart', DateTimeType::class, array(
                'label' => "Date de début",
                'widget' => 'single_text',
                // do not render as type="date", to avoid HTML5 date pickers
                //'html5' => false,
                'date_format' => 'dd-MM-yyyy HH:mm',
                'required' => true,
            ))
            ->add('dateend', DateTimeType::class, array(
                'label' => "Date de fin",
                'widget' => 'single_text',
                'date_format' => 'gg-MM-yyyy HH:mm',
                'required' => false,
            ))
            ->add('minlevel', IntegerType::class, array(
                'label' => "Lvl minimum",
                'attr' => [
                    'min' => 1,
                    'max' => 60,
                    'step' =>1,
                ],
                'required' => false,
            ))
            ->add('maxlevel', IntegerType::class, array(
                'label' => "Lvl maximum",
                'attr' => [
                    'min' => 1,
                    'max' => 60,
                    'step' =>1,
                ],
                'required' => false,
            ))
            ->add('condition', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Conditions requises',
                'required'  => false,
            ))
            ->add('description', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => "Description de l'événement",
                'required'  => false,
            ))
            ->add('strategylink', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Liens stratégies',
                'required'  => false,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_event';
    }


}
