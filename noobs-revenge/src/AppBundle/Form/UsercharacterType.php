<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Characterclass;
use AppBundle\Entity\Characterrole;
use AppBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use AppBundle\Form\Transformer\EntityHiddenTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UsercharacterType
 * @package AppBundle\Form
 */
class UsercharacterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * JustAFormType constructor.
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('charactername', TextType::class, array(
            'label' => 'Pseudo du personnage',
            'required' => true,
        ))
            ->add('level', IntegerType::class, array(
                'label' => "Niveau du personnage",
                'attr' => [
                    'min' => 0,
                    'max' => 60,
                    'step' =>1,
                ],
                'required' => true,
            ))
            ->add('sexe', ChoiceType::class, array(
                'label' => 'Sexe apparent ?',
                'choices' => array('Oui' => true, 'Non' => false),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ))
            ->add('raceid',  EntityType::class, array(
                'label' => 'Race du personnage',
                'class' => 'AppBundle:Race',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
            ))
            ->add('classid',  EntityType::class, array(
                'label' => 'Classe du personnage',
                'class' => 'AppBundle:Characterclass',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
            ))
            ->add('characterfavouriteroleid', EntityType::class, array(
                'label' => 'Rôle préféré avec ce personnage',
                'class' => 'AppBundle:Characterrole',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
            ))
            ->add('job', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Métiers du personnage',
                'required'  => false,
            ))
            ->add('history', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => '(RP) Histoire du personnage',
                'required'  => false,
            ))
            ;

        //if user dont have character => main by default
        $userAlreadyHaveMainCharacter = $options['userAlreadyHaveMainCharacter'];

        if ($userAlreadyHaveMainCharacter) {
            $builder->add('main', ChoiceType::class, array(
                    'label'    => 'Personnage principal ?',
                    'choices' => array('Oui' => true, 'Non' => false),
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
            ));
        } else {
            $builder->add('main', HiddenType::class, array(
                'data' => true,
                'required' => true,
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usercharacter',
            'userAlreadyHaveMainCharacter' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_usercharacter';
    }


}
