<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserprofileidType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nickname',TextType::class, array(
        //'attr' => array('class' => 'tinymce'),
        'label' => 'Pseudo',
        'required'  => true,
    ))
            ->add('professionig', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Profession IG',
                'required'  => false,
            ))
            ->add('description', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Description',
                'required'  => false,
            ))
            ->add('professionirl', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Profession IRL',
                'required'  => false,
            ))
            ->add('personnalwebsite', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Site internet personnel',
                'required'  => false,
            ))
            ->add('favouritemusic', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Musiques préférées',
                'required'  => false,
            ))
            ->add('favouritemovie', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Films préférés',
                'required'  => false,
            ))
            ->add('favouritebook', TextareaType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => 'Livres préférés',
                'required'  => false,
            ));
//            ->add('imagepath')
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Userprofileid'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_userprofileid';
    }


}
