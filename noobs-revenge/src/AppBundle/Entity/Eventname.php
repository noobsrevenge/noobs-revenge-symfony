<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventname
 *
 * @ORM\Table(name="eventname", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_eventName_eventTypeId_idx", columns={"eventTypeId"})})
 * @ORM\Entity
 */
class Eventname
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultImagePath", type="string", length=255, nullable=true)
     */
    private $defaultimagepath;

    /**
     * @var \AppBundle\Entity\Eventtype
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Eventtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eventTypeId", referencedColumnName="id")
     * })
     */
    private $eventtypeid;


}

