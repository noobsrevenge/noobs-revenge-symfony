<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Userprofileid
 *
 * @UniqueEntity(fields="nickname",message="Ce pseudo de profil est déjà pris.")
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserprofileidRepository")
 * @ORM\Table(name="userprofileid", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_userProfileId_userId_idx", columns={"userId"}), @ORM\Index(name="fk_userProfileId_userRoleId_idx", columns={"userRoleId"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Userprofileid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Le pseudo de l'utilisateur doit au minimum faire {{ limit }} caractères de long",
     *      maxMessage = "Le pseudo de l'utilisateur doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="nickname", type="string", length=50, nullable=false)
     */
    protected $nickname;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 2000,
     *      maxMessage = "Le texte sur le métier IRL doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="professionIrl", type="text", nullable=true)
     */
    protected $professionirl;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 2000,
     *      maxMessage = "Le texte sur les métiers IG doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="professionIg", type="text", nullable=true)
     */
    protected $professionig;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Le texte sur le site personnel doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="personnalWebsite", type="text", nullable=true)
     */
    protected $personnalwebsite;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 5000,
     *      maxMessage = "Le texte sur la déscription du joueur doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 10000,
     *      maxMessage = "Le texte sur les musiques préférées doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="favouriteMusic", type="text", nullable=true)
     */
    protected $favouritemusic;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 10000,
     *      maxMessage = "Le texte sur les films préférés doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="favouriteMovie", type="text", nullable=true)
     */
    protected $favouritemovie;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 10000,
     *      maxMessage = "Le texte sur les livres préférés doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="favouriteBook", type="text", nullable=true)
     */
    protected $favouritebook;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateUpdate", type="timestamp", nullable=true)
     */
    protected $dateupdate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateCreation", type="timestamp", nullable=true)
     */
    protected $datecreation;

    /**
     * @var string
     *
     * @ORM\Column(name="imagePath", type="string", length=255, nullable=true)
     */
    protected $imagepath;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $userid;

    /**
     * @var \AppBundle\Entity\Userrole
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Userrole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userRoleId", referencedColumnName="id", nullable=true)
     * })
     */
    protected $userroleid;

    /**
     * Userprofileid constructor.
     */
    public function __construct()
    {
        $this->dateupdate = new \DateTime("now");
        //$this->datecreation = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param $nickname
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfessionirl()
    {
        return $this->professionirl;
    }

    /**
     * @param $professionirl
     * @return $this
     */
    public function setProfessionirl($professionirl)
    {
        $this->professionirl = $professionirl;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfessionig()
    {
        return $this->professionig;
    }

    /**
     * @param $professionig
     * @return $this
     */
    public function setProfessionig($professionig)
    {
        $this->professionig = $professionig;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersonnalwebsite()
    {
        return $this->personnalwebsite;
    }

    /**
     * @param $personnalwebsite
     * @return $this
     */
    public function setPersonnalwebsite($personnalwebsite)
    {
        $this->personnalwebsite = $personnalwebsite;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getFavouritemusic()
    {
        return $this->favouritemusic;
    }

    /**
     * @param $favouritemusic
     * @return $this
     */
    public function setFavouritemusic($favouritemusic)
    {
        $this->favouritemusic = $favouritemusic;
        return $this;
    }

    /**
     * @return string
     */
    public function getFavouritemovie()
    {
        return $this->favouritemovie;
    }

    /**
     * @param $favouritemovie
     * @return $this
     */
    public function setFavouritemovie($favouritemovie)
    {
        $this->favouritemovie = $favouritemovie;
        return $this;
    }

    /**
     * @return string
     */
    public function getFavouritebook()
    {
        return $this->favouritebook;
    }

    /**
     * @param $favouritebook
     * @return $this
     */
    public function setFavouritebook($favouritebook)
    {
        $this->favouritebook = $favouritebook;
        return $this;
    }

    /**
     * @return int
     */
    public function getDateupdate()
    {
        return $this->dateupdate;
    }

    /**
     * @return $this
     */
    public function setDateupdate()
    {
        $this->dateupdate = new \DateTime("now");
        return $this;
    }

    /**
     * @return int
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * @return $this
     */
    public function setDatecreation()
    {
        $this->datecreation = new \DateTime("now");
        return $this;
    }

    /**
     * @return string
     */
    public function getImagepath()
    {
        return $this->imagepath;
    }

    /**
     * @param $imagepath
     * @return $this
     */
    public function setImagepath($imagepath)
    {
        $this->imagepath = $imagepath;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \AppBundle\Entity\User $userid
     * @return $this
     */
    public function setUserid($userid = null)
    {
        $this->userid = $userid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserroleid()
    {
        return $this->userroleid;
    }

    /**
     * @param \AppBundle\Entity\Userrole $characterfavouriteroleid
     * @return $this
     */
    public function setUserroleid($userroleid = null)
    {
        $this->userroleid = $userroleid;
        return $this;
    }




}

