<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use MongoDB\BSON\Timestamp;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Usercharacter
 *
 * @UniqueEntity(fields="charactername",message="Ce nom de perso est déjà pris.")
 *
 * @ORM\Table(name="`usercharacter`", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_character_userId_idx", columns={"userId"}), @ORM\Index(name="fk_character_characterRoleId_idx", columns={"characterFavouriteRoleId"}), @ORM\Index(name="fk_character_classId_idx", columns={"classId"}), @ORM\Index(name="fk_character_raceId_idx", columns={"raceId"})})
 * @ORM\Entity
 */
class Usercharacter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var strin
     *
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Le pseudo du personnage doit au minimum faire {{ limit }} caractères de long",
     *      maxMessage = "Le pseudo du personnage doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="characterName", type="string", nullable=false)
     */
    protected $charactername;


    /**
     * @var boolean
     *
     * @ORM\Column(name="sexe", type="boolean", nullable=false)
     */
    protected $sexe = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean", nullable=true)
     */
    protected $main;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en dessous",
     *     value=0)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au dessus",
     *     value=60)
     * @Assert\Type(type="integer")
     *
     * @ORM\Column(name="level", type="smallint", nullable=false)
     */
    protected $level;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 20000,
     *      maxMessage = "Le champs ''métiers'' doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="job", type="text", nullable=true)
     */
    protected $job;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 20000,
     *      maxMessage = "Le champs ''histoire'' doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="history", type="text", nullable=true)
     */
    protected $history;

    /**
     * @var DateTime;
     *
     * @ORM\Column(name="dateUpdate", type="timestamp", nullable=true)
     */
    protected $dateupdate;

    /**
     * @var \AppBundle\Entity\Characterrole
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Characterrole")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="characterFavouriteRoleId", referencedColumnName="id")
     * })
     */
    protected $characterfavouriteroleid;

    /**
     * @var \AppBundle\Entity\Characterclass
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Characterclass")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="classId", referencedColumnName="id")
     * })
     */
    protected $classid;

    /**
     * @var \AppBundle\Entity\Race
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Race")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="raceId", referencedColumnName="id")
     * })
     */
    protected $raceid;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $userid;

    /**
     * Usercharacter constructor.
     */
    public function __construct()
    {
        $this->dateupdate = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCharactername()
    {
        return $this->charactername;
    }

    /**
     * @param string $charactername
     * @return $this
     */
    public function setCharactername($charactername)
    {
        $this->charactername = $charactername;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSexe()
    {
        return $this->sexe;
    }

    /**
     * @param bool $sexe
     * @return $this
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * @param bool $main
     * @return $this
     */
    public function setMain($main)
    {
        $this->main = $main;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param $level
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return int
     */
    public function getDateupdate()
    {
        return $this->dateupdate;
    }

    /**
     * @return $this
     */
    public function setDateupdate()
    {
        $this->dateupdate = new \DateTime("now");
        return $this;
    }

    /**
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param string $job
     * @return $this
     */
    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return string
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param string $history
     * @return $this
     */
    public function setHistory($history)
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacterfavouriteroleid()
    {
        return $this->characterfavouriteroleid;
    }

    /**
     * @param \AppBundle\Entity\Characterrole $characterfavouriteroleid
     * @return $this
     */
    public function setCharacterfavouriteroleid(\AppBundle\Entity\Characterrole $characterfavouriteroleid = null)
    {
        $this->characterfavouriteroleid = $characterfavouriteroleid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClassid()
    {
        return $this->classid;
    }

    /**
     * @param \AppBundle\Entity\Characterclass $classid
     * @return $this
     */
    public function setClassid(\AppBundle\Entity\Characterclass $classid = null)
    {
        $this->classid = $classid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRaceid()
    {
        return $this->raceid;
    }

    /**
     * @param \AppBundle\Entity\Race $raceid
     * @return $this
     */
    public function setRaceid(\AppBundle\Entity\Race $raceid = null)
    {
        $this->raceid = $raceid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \AppBundle\Entity\User $userid
     * @return $this
     */
    public function setUserid(\AppBundle\Entity\User $userid = null)
    {
        $this->userid = $userid;
        return $this;
    }

    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->charactername;
    }
}

