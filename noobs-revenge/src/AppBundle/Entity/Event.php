<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use MongoDB\BSON\Timestamp;

/**
 * Event
 *
 * @ORM\Table(name="`event`", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_event_authorId_idx", columns={"authorId"}), @ORM\Index(name="fk_event_responsableId_idx", columns={"responsableId"}), @ORM\Index(name="fk_event_eventType_idx", columns={"eventTypeId"})})
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="`id`", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`title`", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="`place`", type="string", length=255, nullable=false)
     */
    protected $place;

    /**
     * @var \DateTime
     *
     * @Assert\GreaterThan("+1 hours")
     *
     * @ORM\Column(name="`dateStart`", type="datetime", nullable=false)
     */
    protected $datestart;

    /**
     * @var \DateTime
     *
     * @Assert\GreaterThan("+2 hours")
     *
     * @ORM\Column(name="`dateEnd`", type="datetime", nullable=true)
     */
    protected $dateend;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 2000,
     *      maxMessage = "Le champs ''Description'' doit au maximum faire {{ limit }} caractères de long. Soyez concis !"
     * )
     *
     * @ORM\Column(name="`description`", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 500,
     *      maxMessage = "Le champs ''Conditions'' doit au maximum faire {{ limit }} caractères de long. Soyez concis !"
     * )
     *
     * @ORM\Column(name="`condition`", type="text", nullable=true)
     */
    protected $condition;

    /**
     * @var integer
     *
     *@Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en dessous",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au dessus",
     *     value=60)
     * @Assert\Type(type="integer")
     *
     * @ORM\Column(name="`minLevel`", type="smallint", nullable=true)
     */
    protected $minlevel;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en dessous",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au dessus",
     *     value=60)
     * @Assert\Type(type="integer")
     *
     * @ORM\Column(name="`maxLevel`", type="smallint", nullable=true)
     */
    protected $maxlevel;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 1000,
     *      maxMessage = "Le champs ''Lien(s) stratégie'' doit au maximum faire {{ limit }} caractères de long. Soyez concis !"
     * )
     *
     * @ORM\Column(name="`strategyLink`", type="text", nullable=true)
     */
    protected $strategylink;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="`dateCreation`", type="timestamp", nullable=true)
     */
    protected $datecreation;

    /**
     * @var \AppBundle\Entity\Eventtype
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Eventtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="`eventTypeId`", referencedColumnName="id")
     * })
     */
    protected $eventtypeid;

    /**
     * @var \AppBundle\Entity\Userprofileid
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Userprofileid")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="`authorId`", referencedColumnName="id")
     * })
     */
    protected $authorid;

    /**
     * @var \AppBundle\Entity\Userprofileid
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Userprofileid")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="`responsableId`", referencedColumnName="id")
     * })
     */
    protected $responsableid;

    protected $participations;

    protected $isUserSubscribed;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->datecreation = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param $place
     * @return $this
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDatestart()
    {
        return $this->datestart;
    }

    /**
     * @param $datestart
     * @return $this
     */
    public function setDatestart($datestart)
    {
        $this->datestart = $datestart;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateend()
    {
        return $this->dateend;
    }

    /**
     * @param $dateend
     * @return $this
     */
    public function setDateend($dateend)
    {
        $this->dateend = $dateend;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param $condition
     * @return $this
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinlevel()
    {
        return $this->minlevel;
    }

    /**
     * @param $minlevel
     * @return $this
     */
    public function setMinlevel($minlevel)
    {
        $this->minlevel = $minlevel;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxlevel()
    {
        return $this->maxlevel;
    }

    /**
     * @param $maxlevel
     * @return $this
     */
    public function setMaxlevel($maxlevel)
    {
        $this->maxlevel = $maxlevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getStrategylink()
    {
        return $this->strategylink;
    }

    /**
     * @param $strategylink
     * @return $this
     */
    public function setStrategylink($strategylink)
    {
        $this->strategylink = $strategylink;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * @return int
     */
    public function setDatecreation()
    {
        $this->datecreation = new \DateTime("now");
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventtypeid()
    {
        return $this->eventtypeid;
    }

    /**
     * @param \AppBundle\Entity\Eventtype $eventtypeid
     * @return $this
     */
    public function setEventtypeid($eventtypeid = null)
    {
        $this->eventtypeid = $eventtypeid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorid()
    {
        return $this->authorid;
    }

    /**
     * @param \AppBundle\Entity\Userprofileid $authorid
     * @return $this
     */
    public function setAuthorid($authorid = null)
    {
        $this->authorid = $authorid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponsableid()
    {
        return $this->responsableid;
    }

    /**
     * @param \AppBundle\Entity\Userprofileid $responsableid
     * @return $this
     */
    public function setResponsableid($responsableid = null)
    {
        $this->responsableid = $responsableid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisUserSubscribed()
    {
        return $this->isUserSubscribed;
    }

    /**
     * @param mixed $isUserSubscribed
     */
    public function setIsUserSubscribed($isUserSubscribed)
    {
        $this->isUserSubscribed = $isUserSubscribed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * @param mixed $participations
     */
    public function setParticipations($participations)
    {
        $this->participations = $participations;
        return $this;
    }


    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->place;
    }
}

