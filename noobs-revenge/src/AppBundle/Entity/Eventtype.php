<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventtype
 *
 * @ORM\Table(name="eventtype")
 * @ORM\Entity
 */
class Eventtype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @ORM\Column(name="kind", type="string", length=50, nullable=true)
     */
    protected $kind;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultImagePath", type="string", length=255, nullable=true)
     */
    protected $defaultimagepath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind($kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDefaultimagepath()
    {
        return $this->defaultimagepath;
    }

    /**
     * @param string $defaultimagepath
     */
    public function setDefaultimagepath($defaultimagepath)
    {
        $this->defaultimagepath = $defaultimagepath;
    }

    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->label;
    }

}

