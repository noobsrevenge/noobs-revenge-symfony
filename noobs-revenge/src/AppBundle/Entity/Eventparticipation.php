<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use MongoDB\BSON\Timestamp;

/**
 * Eventparticipation
 *
 * @ORM\Table(name="eventparticipation", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_eventParticipation_userId_idx", columns={"userId"}), @ORM\Index(name="fk_eventParticipation_characterId_idx", columns={"characterId"}), @ORM\Index(name="fk_eventParticipation_eventId_idx", columns={"eventId"}), @ORM\Index(name="fk_eventParticipation_chosenRoleId_idx", columns={"chosenRoleId"})})
 * @ORM\Entity
 */
class Eventparticipation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="timestamp", nullable=true)
     */
    protected $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="rankNumber", type="smallint", nullable=true)
     */
    protected $ranknumber = 1;

    /**
     * @var \AppBundle\Entity\Usercharacter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usercharacter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="characterId", referencedColumnName="id")
     * })
     */
    protected $characterid;

    /**
     * @var \AppBundle\Entity\Characterrole
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Characterrole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chosenRoleId", referencedColumnName="id")
     * })
     */
    protected $chosenroleid;

    /**
     * @var \AppBundle\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eventId", referencedColumnName="id")
     * })
     */
    protected $eventid;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     */
    protected $userid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime("now");
        $this->ranknumber = 1;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setDate()
    {
        $this->date = new \DateTime("now");
        return $this;
    }

    /**
     * @return int
     */
    public function getRanknumber()
    {
        return $this->ranknumber;
    }

    /**
     * @param $ranknumber
     * @return $this
     */
    public function setRanknumber($ranknumber)
    {
        $this->ranknumber = $ranknumber;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacterid()
    {
        return $this->characterid;
    }

    /**
     * @param \AppBundle\Entity\Usercharacter $characterid
     * @return $this
     */
    public function setCharacterid($characterid)
    {
        $this->characterid = $characterid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChosenroleid()
    {
        return $this->chosenroleid;
    }

    /**
     * @param \AppBundle\Entity\Userrole $chosenroleid
     * @return $this
     */
    public function setChosenroleid($chosenroleid)
    {
        $this->chosenroleid = $chosenroleid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventid()
    {
        return $this->eventid;
    }

    /**
     * @param \AppBundle\Entity\Event $eventid
     * @return $this
     */
    public function setEventid($eventid)
    {
        $this->eventid = $eventid;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \AppBundle\Entity\User $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
        return $this;
    }


}

