<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class
 *
 * @ORM\Table(name="class", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class Characterclass
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onlyAlliance", type="boolean", nullable=true)
     */
    private $onlyalliance = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="onlyHorde", type="boolean", nullable=true)
     */
    private $onlyhorde = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="imagePath", type="string", length=255, nullable=true)
     */
    private $imagepath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return bool
     */
    public function isOnlyalliance()
    {
        return $this->onlyalliance;
    }

    /**
     * @param bool $onlyalliance
     */
    public function setOnlyalliance($onlyalliance)
    {
        $this->onlyalliance = $onlyalliance;
    }

    /**
     * @return bool
     */
    public function isOnlyhorde()
    {
        return $this->onlyhorde;
    }

    /**
     * @param bool $onlyhorde
     */
    public function setOnlyhorde($onlyhorde)
    {
        $this->onlyhorde = $onlyhorde;
    }

    /**
     * @return string
     */
    public function getImagepath()
    {
        return $this->imagepath;
    }

    /**
     * @param string $imagepath
     */
    public function setImagepath($imagepath)
    {
        $this->imagepath = $imagepath;
    }

    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->label;
    }
}

