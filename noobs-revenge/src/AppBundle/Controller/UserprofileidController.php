<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Userprofileid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;
use AppBundle\Entity\Userrole;

/**
 * Userprofileid controller.
 *
 * @Route("userprofileid")
 */
class UserprofileidController extends Controller
{
    /**
     * Lists all userprofileid entities.
     *
     * @Route("/", name="userprofileid_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userprofileids = $em->getRepository('AppBundle:Userprofileid')->findAll();

        return $this->render('userprofileid/index.html.twig', array(
            'userprofileids' => $userprofileids,
        ));
    }

    /**
     * Creates a new userprofileid entity.
     *
     * @param Request $request
     * @param null $user
     *
     * @Route("/new", name="userprofileid_new")
     * @Method({"GET", "POST"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $user = null)
    {
        $userprofileid = new Userprofileid();

        $user = $this->getUser();
        $userId = $user->getId();

        if (!$this->userAlreadyHaveProfile($userId)) {
            $this->addFlash("warning", "Vous avez déjà un profil");
            return $this->redirectToRoute('site_index');
        }

        $form = $this->createForm('AppBundle\Form\UserprofileidType', $userprofileid);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //hidden fields
            $userprofileid->setDatecreation();
            $userprofileid->setDateupdate();
            $userprofileid->setUserid($user);

            $em->persist($userprofileid);
            $em->flush();

            return $this->redirectToRoute('usercharacter_new');
        }

        return $this->render('userprofileid/new.html.twig', array(
            'userprofileid' => $userprofileid,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userprofileid entity.
     *
     * @Route("/{id}", name="userprofileid_show")
     * @Method("GET")
     */
    public function showAction(Userprofileid $userprofileid)
    {
        return $this->render('userprofileid/show.html.twig', array(
            'userprofileid' => $userprofileid,
        ));
    }

    /**
     * Displays a form to edit an existing userprofileid entity.
     *
     * @Route("/{id}/edit", name="userprofileid_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Userprofileid $userprofileid)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($userprofileid->getUserid()->getId() != $userId) {
            $this->addFlash("danger", "Ce profil ne vous appartient pas.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $editForm = $this->createForm('AppBundle\Form\UserprofileidType', $userprofileid);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $userprofileid->setDateupdate();

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('userprofileid_edit', array('id' => $userprofileid->getId()));
        }

        return $this->render('userprofileid/edit.html.twig', array(
            'userprofileid' => $userprofileid,
            'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * @param $userid
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProfileAction(Request $request)
    {
        $userid = $request->attributes->get('id');

        $em = $this->getDoctrine()->getManager();

        $userprofileid = $em->getRepository('AppBundle:Userprofileid')->findBy(['userid' => $userid]);
        $userprofileid = $userprofileid[0];

        $editForm = $this->createForm('AppBundle\Form\UserprofileidType', $userprofileid);
        $editForm->handleRequest($request);


        return $this->render('userprofileid/edit.html.twig', array(
            'userprofileid' => $userprofileid,
            'edit_form' => $editForm->createView(),
        ));
    }

    #region aide SQL
    /**
     *
     * @param null $userId
     * @return bool
     */
    public function userAlreadyHaveProfile($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction userAlreadyHaveProfile - $.userId=null - ");
        }
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(up) FROM AppBundle:Userprofileid up 
              INNER JOIN AppBundle:User AS u 
              WITH u.id = up.userid 
              WHERE up.userid=' . $userId
        );

        $profile = $query->getResult();

        if ($profile > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function getUserprofileidByUserId ($userId = null) {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserprofileidByUserId - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT up FROM AppBundle:Userprofileid up 
                WHERE up.userid=' . $userId
        );

        $nickname = $query->getResult();

        if (!empty($nickname)) {
            return $nickname[0]["nickname"];
        } else {
            return false;
        }
    }
    #endregion

}
