<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Event;
use AppBundle\Entity\Eventparticipation;
use AppBundle\Entity\Userprofileid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;


class EventparticipationController extends Controller
{
//    /**
//     * @param $eventId
//     * @return array
//     */
//    public function getParticipantsToEventAction($eventId)
//    {
//        /** @var ParticipantRepository $repo */
//        $repo = $this->getDoctrine()->getManager()->getRepository(Eventparticipation::class);
//        $participants = $repo->findBy(['eventid' => $eventId]);
//    }

    /**
     * @param Request $request
     * @param null $eventid
     *
     */
    public function addParticipantAction(Request $request, $eventid = null)
    {
        $eventid = $request->attributes->get('id');
        $eventid = intval($eventid);

        $participation = new Eventparticipation();

        $user = $this->getUser();
        $userId = $user->getId();

        //testing if user have one character at least and get userprodileidid
        $userProfileidId = $this->getUserProfileidId($userId);
        if (!$userProfileidId) {
            $this->addFlash("danger", "Vous devez créer un profil avant de pouvoir participer à un événement.");
            return $this->redirectToRoute('userprofileid_new', array('user' => $user));
        }
        $charactersNumber = $this->getUserHaveOneCharacterAtLeast($userId);
        if (!$charactersNumber) {
            $this->addFlash("danger", "Vous devez créer un personnage avant de participer à un événement.");
            return $this->redirectToRoute('usercharacter_new', array('user' => $user));
        }

        $options = array(
            'userid' => $userId,
        );

        $form = $this->createForm('AppBundle\Form\EventparticipationType', $participation, $options);
        $form->handleRequest($request);

        $idcharacter = intval($request->request->get('character'));
        if ($idcharacter == null) {
            $this->addFlash('danger', 'Vous devez choisir un personnage à inscrire');
            return $this->redirectToRoute('event_index');
        }


        $character = $this->getCharacterByCharcterId($idcharacter);

        $idrole = intval($request->request->get('selectorrole'));
        if ($idrole == null) {
            $this->addFlash('danger', 'Vous devez choisir un rôle pour vous inscrire à cet événement.');
            return $this->redirectToRoute('event_index');
        }

        $role = $this->getRoleByRoleId($idrole);

        $participation->setCharacterid($character);
        $participation->setChosenroleid($role);

        //hidden fields
       $participation->setUserid($user);
       $participation->setDate();

       $event = $this->getEventByEventId($eventid);

       if (!$event) {
           $this->addFlash("danger", "ERROR idevent - Evenement non trouvé.");
           return $this->redirectToRoute('event_index');
       }

       $participation->setEventid($event);

       $lastRankNumber = 1;
       $lastRankNumber = $this->getLastRankNumberForEvent($eventid);

       if (!$lastRankNumber) {
           $lastRankNumber = 1;
       }

       $participation->setRanknumber($lastRankNumber);

       $em = $this->getDoctrine()->getManager();
       $em->persist($participation);
       $em->flush();

        $this->addFlash('success', 'Vous participez maintenant à cet évènement');
        return $this->redirectToRoute('event_index');
    }
//
    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeParticipantAction($id, Request $request)
    {
        $eventid = $request->attributes->get('id');
        $eventid = intval($eventid);
        $event = $this->getEventByEventId($eventid);

        if (!$event) {
            $this->addFlash("danger", "ERROR idevent - Evenement non trouvé.");
            return $this->redirectToRoute('event_index');
        }

        $user = $this->getUser();
        $userId = $user->getId();

        $em = $this->getDoctrine()->getManager();

        $participant = $em->getRepository(Eventparticipation::class)->findOneBy(['userid' => $user, 'eventid' => $event ]);

        $em->remove($participant);
        $em->persist($event);
        $em->flush();


        $this->addFlash('success', 'Vous ne participez plus à cet évènement');
        return $this->redirectToRoute('event_index');
    }


    /**
 *
 * @param null $userId
 * @return bool
 */
    public function getUserProfileidId($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserProfileidId - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT up.id FROM AppBundle:Userprofileid up 
                INNER JOIN AppBundle:User AS u 
                WITH u.id = up.userid 
                WHERE up.userid=' . $userId
        );

        $userProfileidId = $query->getResult();

        if (!empty($userProfileidId)) {
            $userProfileidId = $userProfileidId[0]["id"];
            if (intval($userProfileidId) > 0) {
                return $userProfileidId;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     *
     * @param null $userId
     * @return bool
     */
    public function getUserHaveOneCharacterAtLeast($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserHaveOneCharacterAtLeast - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userId
        );

        $charactersNumber = $query->getResult();

        if (intval($charactersNumber[0][1]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $eventid
     * @return bool
     */
    public function getLastRankNumberForEvent($eventid = null)
    {
        if ($eventid == null) {
            throw new Exception(" - Fonction getLastRankNumberForEvent - $.eventid=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT ep FROM AppBundle:Eventparticipation ep 
                WHERE ep.eventid=' . $eventid .
                'ORDER BY ep.ranknumber DESC'
        );

        $lastRankNumber = $query->getResult();

        if (!empty($lastRankNumber)) {
            $lastRankNumber = $lastRankNumber[0];
            $lastRankNumber = $lastRankNumber->getRanknumber();
            return $lastRankNumber;
        } else {
            return false;
        }
    }

    /**
     * @param null $characterid
     * @return bool
     */
    public function getCharacterByCharcterId($characterid = null)
    {
//        if ($characterid == null) {
//            throw new Exception(" - Fonction getCharacterByCharcterId - $.characterid = null - ");
//        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT uc FROM AppBundle:Usercharacter uc 
                WHERE uc.id=' . $characterid
        );

        $usercharacter = $query->getResult();


        if (!empty($usercharacter)) {
            return $usercharacter[0];
        } else {
            return false;
        }
    }

    /**
     * @param null $roleid
     * @return bool
     */
    public function getRoleByRoleId($roleid = null)
    {
        if ($roleid == null) {
            throw new Exception(" - Fonction getRoleByRoleId - $.roleid = null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT ur FROM AppBundle:Characterrole ur 
                WHERE ur.id=' . $roleid
        );

        $role = $query->getResult();

        if (!empty($role)) {
            return $role[0];
        } else {
            return false;
        }
    }

    /**
     * @param null $eventid
     * @return bool
     */
    public function getEventByEventId($eventid = null)
    {
        if ($eventid == null) {
            throw new Exception(" - Fonction getEventByEventId - $.eventid = null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT e FROM AppBundle:Event e 
                WHERE e.id=' . $eventid
        );

        $event = $query->getResult();

        if (!empty($event)) {
            return $event[0];
        } else {
            return false;
        }
    }
}