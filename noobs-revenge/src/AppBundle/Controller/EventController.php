<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Event;
use AppBundle\Entity\Userprofileid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Eventparticipation;

/**
 * Event controller.
 *
 * @Route("event")
 */
class EventController extends Controller
{
    /**
     * Lists all event entities.
     */
    public function indexAction()
    {
        $participation = new Eventparticipation();

        $user = $this->getUser();
        $userId = $user->getId();

        $options = array(
            'userid' => $userId,
        );

        $form = $this->createForm('AppBundle\Form\EventparticipationType', $participation, $options);

        $usercharacters = $this->getAllDataAction($userId);

        if (!$usercharacters) {
            $this->addFlash("danger", "Vous n'avez aucun personnage, vous ne pouvez donc pas vous inscrire aux événements. Créez votre personnage grâce au menu.");
        }

        $em = $this->getDoctrine()->getManager();

        //Events started and not finished, or not started yet, ordered by "next"
        $query = $em->createQuery(
            'SELECT e FROM AppBundle:Event e  
                WHERE e.datestart > CURRENT_DATE()
                OR (e.datestart < CURRENT_DATE() AND e.dateend > CURRENT_DATE())
                ORDER BY e.datestart ASC'
        );

        $events = $query->getResult();

        if (empty($events)) {
            $this->addFlash("warning", "Il n'y a pas d'événement en cours ou d'événement prévu pour l'instant. Proposez en un.");
            return $this->render('event/index.html.twig');
        }

        //TODO RAJOUTER PARTICIPANTS DANS EVENT TAB, envoyer isSubscribed au template pour chaque event
        foreach ($events as $event) {
            $eventId = $event->getId();
            $participants = $this->getAllEventParticipantAction($eventId);
            $isUserSubscribed = $this->getIsUserSubscribedAction($userId, $eventId);
            $event->setIsUserSubscribed($isUserSubscribed);
            $event->setParticipations($participants);
        }



        return $this->render('event/index.html.twig', array(
            'events' => $events,
            'usercharacters' => $usercharacters,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new event entity.
     */
    public function newAction(Request $request)
    {
        $event = new Event();

        $user = $this->getUser();
        $userId = $user->getId();

        //testing if user have one character at least and get userprodileidid
        $userProfileidId = $this->getUserProfileidId($userId);
        if (!$userProfileidId) {
            $this->addFlash("danger", "Vous devez créer un profil avant de pouvoir créer un événement.");
            return $this->redirectToRoute('userprofileid_new', array('user' => $user));
        }

        //get Userprofileid object
        $userprofileid = $this->getDoctrine()
            ->getRepository(Userprofileid::class)
            ->find($userProfileidId);

        if (!$userprofileid) {
            throw $this->createNotFoundException(
                'No UserProfile found for id '.$userprofileid
            );
        }

        $form = $this->createForm('AppBundle\Form\EventType', $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //hidden fields
            $event->setAuthorid($userprofileid);
            $event->setResponsableid($userprofileid);
            $event->setDatecreation();

            //swap min/max level if lesserthan
            if (!is_null($event->getMinlevel()) && !is_null($event->getMaxlevel())) {
                if ($event->getMaxlevel() < $event->getMinlevel()) {
                    $temp = $event->getMinlevel();
                    $event->setMinlevel($event->getMaxlevel());
                    $event->setMaxlevel($temp);
                    $temp = null;
                }
            }

            //swap date end/date debut if lesserthan
            if (!is_null($event->getDateend())) {
                if ($event->getDateend() < $event->getDatestart()) {
                    $temp = $event->getDatestart();
                    $event->setDatestart($event->getDateend());
                    $event->setDateend($temp);
                }
            }

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('event_index');
        }

        return $this->render('event/new.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a event entity.
     *
     */
    public function showAction(Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('event/show.html.twig', array(
            'event' => $event,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     */
    public function editAction(Request $request, Event $event)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        $userProfileidId = $this->getUserProfileidId($userId);
        /** @var Userprofileid $userprofileid */
        $userprofileid = $this->getDoctrine()
            ->getRepository(Userprofileid::class)
            ->find($userProfileidId);

        if ($event->getAuthorid()->getId() != $userprofileid->getUserid()->getId()) {
            $this->addFlash("danger", "Vous n'êtes pas l'auteur de cet événement. Vous ne pouvez donc pas le modifier ni le supprimer.");
            return $this->redirectToRoute('event_index', array('user' => $user));
        }

        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm('AppBundle\Form\EventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            //swap min/max level if lesserthan
            if (!is_null($event->getMinlevel()) && !is_null($event->getMaxlevel())) {
                if ($event->getMaxlevel() < $event->getMinlevel()) {
                    $temp = $event->getMinlevel();
                    $event->setMinlevel($event->getMaxlevel());
                    $event->setMaxlevel($temp);
                    $temp = null;
                }
            }

            //swap date end/date debut if lesserthan
            if (!is_null($event->getDateend())) {
                if ($event->getDateend() < $event->getDatestart()) {
                    $temp = $event->getDatestart();
                    $event->setDatestart($event->getDateend());
                    $event->setDateend($temp);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_edit', array('id' => $event->getId()));
        }

        return $this->render('event/edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     */
    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('event_index');
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param Event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    #region aide SQL
    /**
     *
     * @param null $userId
     * @return bool
     */
    public function userAlreadyOneCharacter($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction userAlreadyOneCharacter - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:Usercharacter uc 
                INNER JOIN AppBundle:User AS u 
                WITH u.id = uc.userid 
                WHERE uc.userid=' . $userId
        );

        $userCharacterNumber = $query->getResult();

        if (intval($userCharacterNumber[0][1]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param null $userId
     * @return bool
     */
    public function getUserProfileidId($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserProfileidId - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT up.id FROM AppBundle:Userprofileid up 
                INNER JOIN AppBundle:User AS u 
                WITH u.id = up.userid 
                WHERE up.userid=' . $userId
        );

        $userProfileidId = $query->getResult();

        if (!empty($userProfileidId)) {
            $userProfileidId = $userProfileidId[0]["id"];
            if (intval($userProfileidId) > 0) {
                return $userProfileidId;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
 * param int $userid
 * @return mixed
 */
    public function getAllDataAction($userid = null)
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT uc FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userid
            . ' ORDER BY uc.main DESC'
        );

        $usercharacters = $query->getResult();

        if (count($usercharacters) > 0) {
            return $usercharacters;
        } else {
            return false;
        }
    }

    /**
     * param int $userid
     * @return mixed
     */
    public function getAllEventParticipantAction($eventid = null)
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT ep FROM AppBundle:Eventparticipation ep 
                WHERE ep.eventid=' . $eventid
            . ' ORDER BY ep.date DESC'
        );

        $participants = $query->getResult();

        if (count($participants) > 0) {
            return $participants;
        } else {
            return false;
        }
    }

    /**
     * @param null $userid
     * @param null $eventid
     * @return bool
     */
    public function getIsUserSubscribedAction($userid = null, $eventid = null)
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT count(ep) FROM AppBundle:Eventparticipation ep 
                WHERE ep.userid=' . $userid .
                'AND ep.eventid=' . $eventid
        );

        $subscribed = $query->getResult();

        if (intval($subscribed[0][1]) > 0) {
            return true;
        } else {
            return false;
        }
    }
    #endregion
}
