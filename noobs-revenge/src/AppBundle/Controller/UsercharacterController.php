<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usercharacter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Characterrole;
use AppBundle\Entity\Characterclass;
use AppBundle\Entity\Characterrace;
use AppBundle\Entity\User;
use AppBundle\Entity\Userprofileid;

/**
 * Usercharacter controller.
 *
 * @Route("usercharacter")
 */
class UsercharacterController extends Controller
{
    /**
     * Lists all usercharacter entities.
     *
     * @Route("/", name="usercharacter_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $characters = $em->getRepository('AppBundle:Usercharacter')->findAll();

        return $this->render('usercharacter/index.html.twig', array(
            'characters' => $characters,
        ));
    }

    #region form CRUD
    /**
     * Creates a new usercharacter entity.
     *
     * @param Request $request
     * @param null $user
     *
     * @Route("/new", name="usercharacter_new")
     * @Method({"GET", "POST"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $user = null)
    {
        $character = new Usercharacter();

        $user = $this->getUser();
        $userId = $user->getId();

        $alreadyTenCharacters = $this->userHaveAlreadyTenCharacters($userId);
        if ($alreadyTenCharacters) {
            $this->addFlash("danger", "Vous avez déjà 10 personnages enregistrés. Supprimez ou remplacez en un.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $alreadyMain = $this->userAlreadyHaveMainCharacter($userId);

        $options = array(
            'userAlreadyHaveMainCharacter' => $alreadyMain,
        );

        $form = $this->createForm('AppBundle\Form\UsercharacterType', $character, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //hidden fields
            $character->setUserid($user);
            $character->setDateupdate();

            //if main character chosen, we update others
            if ($character->isMain()) {
                $this->putEveryCharactersNotMain($userId);
            }

            $em->persist($character);
            $em->flush();

            $this->addFlash("success", "Personnage créé avec succès.");
            return $this->redirectToRoute('index');
        }

        return $this->render('usercharacter/new.html.twig', array(
            'character' => $character,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a usercharacter entity.
     *
     * @Route("/{id}", name="usercharacter_show")
     * @Method("GET")
     */
    public function showAction(Usercharacter $character)
    {
        $deleteForm = $this->createDeleteForm($character);

        return $this->render('usercharacter/show.html.twig', array(
            'character' => $character,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing usercharacter entity.
     *
     * @Route("/{id}/edit", name="usercharacter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Usercharacter $character)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($character->getUserid()->getId() != $userId) {
            $this->addFlash("danger", "Ce personnage ne vous appartient pas.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $deleteForm = $this->createDeleteForm($character);

        // just a paramater to print "main form field"
        $alreadyMain = true;

        $options = array(
            'userAlreadyHaveMainCharacter' => $alreadyMain,
        );

        $editForm = $this->createForm('AppBundle\Form\UsercharacterType', $character, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //hidden fields
            $character->setDateupdate();

            //if main character chosen, we update others
            if ($character->isMain()) {
                $this->putEveryCharactersNotMain($userId);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usercharacter_edit', array('id' => $character->getId()));
        }

        return $this->render('usercharacter/edit.html.twig', array(
            'character' => $character,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a usercharacter entity.
     *
     * @Route("/{id}", name="usercharacter_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, Usercharacter $character)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($character->getUserid()->getId() != $userId) {
            $this->addFlash("danger", "Ce personnage ne vous appartient pas.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $characterId = $character->getId();

        $form = $this->createDeleteForm($character);
        $form->handleRequest($request);

        $previousUrl = $request->headers->get('referer');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Usercharacter')->find($characterId);

        if ($entity instanceof Usercharacter) {
            $em->remove($entity);
            $em->flush();
        } else {
            $this->addFlash("danger", "Erreur lors de la suppression, veuillez contacter l'admin.");
        }

        if (!empty($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirectToRoute('userprofileid_index');
        }
    }

    /**
     * Creates a form to delete a usercharacter entity.
     *
     * @param Usercharacter $character The usercharacter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usercharacter $character)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usercharacter_delete', array('id' => $character->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
    #endregion

    #region actions perso
    /**
     * @param int $userid
     * @return mixed
     *
     * @Route("/{id}", name="usercharacter_getall")
     * @Method("GET")
     */
    public function getAllAction(Request $request, $userid = null)
    {
        $routeParams = $request->attributes->get('_route_params');
        $userid = $routeParams['id'];

        if ($userid == null) {
            throw new Exception(" - Fonction getAllAction - $.userid=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT uc FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userid
            . ' ORDER BY uc.main DESC'
        );

        $usercharacters = $query->getResult();

        $nickname = $this->getUserprofileidNicknameByUserid($userid);

        if (count($usercharacters) > 0 && !empty($nickname)) {
            return $this->render('usercharacter/allusercharacters.html.twig', [
                'userid' => $userid,
                'characters' => $usercharacters,
                'nickname' => $nickname,
            ]);
        } else {
            $this->addFlash("danger", "Cet utilisateur n'a pas encore créé de personnage.");
            return $this->render('userprofileid/index.html.twig');
        }
    }
    #endregion

    #region aide SQL
    /**
     *
     * @param null $userId
     * @return bool
     */
    public function userAlreadyHaveMainCharacter($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction userAlreadyHaveMainCharacter - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userId
            . ' AND uc.main = 1'
        );

        $mainCharacters = $query->getResult();

        if (intval($mainCharacters[0][1]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function putEveryCharactersNotMain($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction putEveryCharactersNotMain - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'UPDATE AppBundle:Usercharacter uc 
                SET uc.main = 0 
                WHERE uc.main = 1 
                AND uc.userid = ' . $userId
        );

        $updateDone = $query->getResult();

        if ($updateDone > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function userHaveAlreadyTenCharacters ($userId = null) {
        if ($userId == null) {
            throw new Exception(" - Fonction userHaveAlreadyTenCharacters - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userId
        );

        $charactersNumber = $query->getResult();

        if ($charactersNumber[0][1] >= 10) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function getUserprofileidNicknameByUserid ($userId = null) {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserprofileidNicknameByUserid - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT up.nickname FROM AppBundle:Userprofileid up 
                WHERE up.userid=' . $userId
        );

        $nickname = $query->getResult();

        if (!empty($nickname)) {
            return $nickname[0]["nickname"];
        } else {
            return false;
        }
    }
    #endregion
}
