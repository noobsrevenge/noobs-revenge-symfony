<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\Usercharacter;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $user = $this->getUser();
        $userId = $user->getId();

        //if user don't have profile, redirection + flash
        $userprofileid = $this->getUserprofileidByUserId($userId);
        if (!$userprofileid) {
            $this->addFlash("danger", "Étape 2 sur 3 - Vous devez créer un profil avant de pouvoir utiliser l'application.");
            return $this->redirectToRoute('userprofileid_new');
        }

        //if user don't have character, redirection + flash
        $usercharacter = $this->countCharacter($userId);


        if ($usercharacter == 'empty') {
            $character = new Usercharacter();

            $options = array(
                'userAlreadyHaveMainCharacter' => false,
            );

            $form = $this->createForm('AppBundle\Form\UsercharacterType', $character, $options);

            $this->addFlash("danger", "Étape 3 sur 3 - Vous devez créer au moins un personnage avant de pouvoir utiliser l'application.");
            return $this->render('usercharacter/new.html.twig', [
                'nocharacter' => true,
                'form' => $form->createView(),
            ]);
            //return $this->redirectToRoute('usercharacter_new', array("nocharacter" => true));
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }



    /**
     * @param null $userId
     * @return bool
     */
    public function getUserprofileidByUserId ($userId = null) {
        if ($userId == null) {
            throw new Exception(" - Fonction getUserprofileidByUserId - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT up FROM AppBundle:Userprofileid up 
                WHERE up.userid=' . $userId
        );

        $userprofileid = $query->getResult();

        if (!empty($userprofileid)) {
            return $userprofileid[0];
        } else {
            return false;
        }
    }

    /**
     * @param int $userid
     * @return mixed
     *
     * @Route("/{id}", name="usercharacter_getall")
     * @Method("GET")
     */
    public function countCharacter($userid = null)
    {
        if ($userid == null) {
            throw new Exception(" - Fonction countCharacterAction - $.userid=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT uc FROM AppBundle:Usercharacter uc 
                WHERE uc.userid=' . $userid
        );

        $usercharacters = $query->getResult();

        if (empty($usercharacters)) {
            return 'empty';
        }
    }
}
